# brainyquote-scraper

brainyquote.com web scraper. creates a json file for each author.

## Requirements

* Node.js

## Installation

* Clone the repo
```
$ git clone https://gitlab.com/ibrahim/brainyquote-scraper.git 
```

* Start web scraping and write output to scrape-out directory
```
$ cd brainyquote-scraper
$ node start
```

## Copyright and License

Copyright (c) 2020 Mohamed Ibrahim. Code released under the [MIT](LICENSE) license.
