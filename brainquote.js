const axios = require('axios');
const cheerio = require('cheerio');
const fs = require('fs');

// NOTE: TOR proxy will not work as brainyquote is blocking TOR IPs

class brainyquote {

    constructor(output_basedir) {
        this.baseurl = "https://www.brainyquote.com";
        this.output_basedir = output_basedir;
        this.charcodes = {};
        this.charcodes['a'] = 'a'.charCodeAt(0);
        this.charcodes['z'] = 'z'.charCodeAt(0);
    }


    async scrape() {
        this.scrape_authors();
    }

    async scrape_authors () {       
        
        for (let i = this.charcodes.a; i<= this.charcodes.z; i++) {
            var alphabet = String.fromCharCode(i);

            var current_page = 1;
            var pages = 1;
            var author_url = '';
            var html = '';
            do {
                if(current_page == 1) {                    
                    author_url = `${this.baseurl}/authors/${alphabet}`
                } 
                else {
                    author_url = `${this.baseurl}/authors/${alphabet}${current_page}`
                }

                html = await this.get_html(author_url);
                var $ = cheerio.load(html)

                if(current_page == 1) {
                    // get number of pages on first page
                    pages = this.get_author_pages($);
                }
                
                var author_list = this.get_author_list($);
                for (let a = 0; a < author_list.length; a++) {
                    let author_data = this.get_author_data_from_author_list(author_list[a]);
                    console.log(author_data);
                    let quotes = await this.get_quotes_by_author(author_data);

                    //await fs.mkdir(`${this.output_basedir}/${alphabet}`, { recursive: true}, (err)=> { });
                    await fs.mkdirSync(`${this.output_basedir}/${alphabet}`, { recursive: true});
                    await this.save_quotes(quotes,`${this.output_basedir}/${alphabet}/${author_data.slug}.json`)
                }


                current_page++;
            } while (current_page <= pages);
        }
    }


    async save_quotes(data, filename) {
        var write_data;
        try {
            let replacer = function (k, v) {
                if (k === "prev" || k === "next" || k === "parent" || k === "_root" ||
                k === "prevObject" || k === "options" || k === "length") return undefined;
                return v;
            };

            write_data = JSON.stringify(data, replacer, 4);
        } catch (e) {
            console.log("json error");
            console.log(e);
        }
        try {
            await fs.writeFile(filename, write_data, (err) => { })
            return true;
        }
        catch (e) {
            console.log(`error saving ${filename}`);
            return false;
        }
    }


    get_author_pages($) {
        var p = $('.bq_s ul.pagination > li')        
        var pages = 1;

        // if pagination list is not found, then assume single page
        if (p.length>0) {
            pages = parseInt(p[p.length - 2].children[0].children[0].data);
        }
                
        return pages;
    }

    get_author_list($) {
        var a = $('.bq_s > table > tbody > tr > td > a')


        console.log(a.length);
        return a;
    }

    get_author_data_from_author_list(data) {
        return { url: `${this.baseurl}${data.attribs.href}`, name: `${data.children[0].data}`, slug: (new String(data.attribs.href)).replace(/\/authors\/(.*)-quotes/,"$1")}
    }


    async get_quotes_by_author(author_data) {
        var quotes = {};
        var current_page = 1;
        var pages = 1;        
        var html = {};

        var quotes_url = author_data.url;                
        var quotes_flags = {};

        do {
            quotes_flags = { get_author_details: false }

            if(current_page == 1) {
                quotes_url = author_data.url;
                quotes_flags = { get_author_details: true }
            }
            else {
                quotes_url = `${author_data.url}_${current_page}`;
            }

            html = await this.get_html(quotes_url);
            var $ = cheerio.load(html)

            if(current_page==1) {
                pages = this.get_quotes_by_author_pages($);
            }
           
            

            let quotes_temp = this.get_quotes($, quotes_flags);
            if(current_page == 1) {
                Object.assign(quotes, quotes_temp);
            }
            else {
                let q = quotes_temp.quotes;
                quotes.quotes.push(...q);
            }


            current_page++;
        }
        while(current_page <= pages);
        
        return quotes;
    }
    

    get_quotes_by_author_pages($) {
        var p = $('.bq_s ul.pagination > li');

        
        var pages = 1;
        
        // if p.length is 0, then selection is not found, probably single page
        if(p.length>1){
            pages = parseInt(p[p.length - 2].children[0].children[0].data)
        }

        return pages;
    }

    get_quotes($, flags) {
        var data = $('.m-brick.grid-item')

        var quotes = data.map( (i,el) => {
            var q = $(el).find('a.b-qt')[0].children[0].data;
            var a = $(el).find('a.bq-aut')[0].children[0].data;            
            var t = $(el).find('a.oncl_list_kc').map( (i,tel) => { 
                return tel.children[0].data;
            }).toArray();
            return { quote: q, author: a, topics: t }
        }).toArray();

        var bio = {};
        if(flags.get_author_details == true) {
            var author_data = $('.subnav-below-p').find('a');
            var author_text = $('.subnav-below-p').text().replace(/\n/g,' '); 
            var n = author_data.map( (i,el) => { if (el.attribs.href.indexOf("nationality")>0) { return el.children[0].data} }) // nationality
            var p = author_data.map( (i,el) => { if (el.attribs.href.indexOf("profession")>0) { return el.children[0].data} })  // profession
 
            var bc = 0 ; var bi = []; 
            var dob = ""; var dod = "";           
            author_data.each(function(i,el) { if(el.attribs.href.indexOf("birthdays")>0) { bi.push(i); bc++; }}) // count number of birthday fields and get array index 
            if(bc>=1) { var re = new RegExp(author_data[bi[0]].children[0].data + ', [0-9]*'); let d = author_text.match(re); if (d) { dob = d[0]; } }
            if(bc>=2) { var re = new RegExp(author_data[bi[1]].children[0].data + ', [0-9]*'); let d = author_text.match(re); if (d) { dod = d[0]; } }
 
            bio = { name: quotes[0].author, nationality: n[0], profession: p[0], date_birth: dob, date_death: dod };             
        }       

        var quotes_data = {};
        quotes_data.author_bio = bio;
        quotes_data.quotes = quotes;

        return quotes_data;
    }

    async get_html (url) {        
        try {
            var response = await axios.get(url);            
            return response.data;
        }
        catch(error) {
            console.log("error - get_html");
        }
        
    }

}

module.exports = brainyquote;

